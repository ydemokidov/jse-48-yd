package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.dto.model.ProjectDTO;
import com.t1.yd.tm.dto.response.AbstractResultResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResultResponse {

    private List<ProjectDTO> projectDTOS;

    public ProjectListResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
