package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlJsonSaveResponse extends AbstractResultResponse {

    public DataFasterXmlJsonSaveResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public DataFasterXmlJsonSaveResponse() {

    }
}
