package com.t1.yd.tm.api.repository.dto;

import com.t1.yd.tm.dto.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IDtoProjectRepository extends IDtoUserOwnedRepository<ProjectDTO> {

    @NotNull List<ProjectDTO> findAll(@NotNull String sort);

}